package com.example.myapplication

data class Person(var name: String, var age: Int, var about: String) {
    constructor() : this("", 0, "")
}

fun main() {
    val Ace = Person()
    val stringDescription = Ace.apply {
        name = "Ace"
        age = 22
        about = "Android developer"
    }.toString()
    println(stringDescription)
}