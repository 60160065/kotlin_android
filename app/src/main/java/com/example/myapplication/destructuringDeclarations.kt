package com.example.myapplication

//**Ex.1
//fun findMinMax(list: List<Int>): Pair<Int, Int> {
//    // do the math
//    return Pair(50, 100)
//}
//
//fun main() {
//    val (x, y, z) = arrayOf(5, 10, 15)
//
//    val map = mapOf("Alice" to 21, "Bob" to 25)
//    for ((name, age) in map) {
//        println("$name is $age years old")
//    }
//
//    val (min, max) = findMinMax(listOf(100, 90, 50, 98, 76, 83))
//
//}

//**Ex.2
//data class Client(val username: String, val email: String)
//
//fun getClient() = Client("Mary", "mary@somewhere.com")
//
//fun main() {
//    val user = getClient()
//    val (username, email) = user
//    println(username == user.component1())
//
//    val (_, emailAddress) = getClient()
//
//}

//**Ex.3
class Pair<K, V>(val first: K, val second: V) {
    operator fun component1(): K {
        return first
    }

    operator fun component2(): V {
        return second
    }
}

fun main() {
    val (num, name) = Pair(1, "one")

    println("num = $num, name = $name")
}