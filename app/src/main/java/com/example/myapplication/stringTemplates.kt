package com.example.myapplication

fun main() {
    val greeting = "Kotliner"

    println("Hello $greeting")
    println("Hello ${greeting.toUpperCase()}")
}